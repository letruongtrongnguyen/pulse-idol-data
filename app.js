let TOKEN =
  "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE1OTMwNzkwODgsInVzZXJfaWQiOjE1MzIwMzMsImlzX2FkbWluIjpmYWxzZSwiZXh0X2luZm8iOnt9LCJyb2xlcyI6W119.T8zhEf9Vkw82GGpjnFWXf3WqUlZVOjyNN028zRIuoxw";
let DEVICE_ID = "c7572ba546ce453c";
// let DEVICE_ID = "Test Device";

const getUserInfo = () => {
  return axios.get(
    "https://stag-pulse-api-v2.mocogateway.com/pulse/v1/user/me",
    {
      headers: {
        Authorization: "Bearer " + TOKEN,
        "Device-Id": DEVICE_ID,
      },
    }
  );
};

const getIdolOverview = (from, to) => {
  return axios.get(
    `https://stag-pulse-api-v2.mocogateway.com/v1/pulse/idol/over-view?from=${from}&to=${to}`,
    {
      headers: {
        Authorization: "Bearer " + TOKEN,
        "Device-Id": DEVICE_ID,
      },
    }
  );
};

const getCallSession = (from, to) => {
  return axios.get(
    `https://stag-pulse-api-v2.mocogateway.com/v1/pulse/idol/call-session?from=${from}&to=${to}`,
    {
      headers: {
        Authorization: "Bearer " + TOKEN,
        "Device-Id": DEVICE_ID,
      },
    }
  );
};

const getFriendRequest = (from, to, userId) => {
  return axios.get(
    `https://stag-pulse-api-v2.mocogateway.com​/v1/pulse/idol/friend-request-statistic?from=${from}&to=${to}&user_id=${userId}`,
    {
      headers: {
        Authorization: "Bearer " + TOKEN,
        "Device-Id": DEVICE_ID,
      },
    }
  );
};

const getUserGift = () => {
  return axios.get(
    "https://stag-pulse-api-v2.mocogateway.com/v1/engagement/user/gift",
    {
      headers: {
        Authorization: "Bearer " + TOKEN,
        "Device-Id": DEVICE_ID,
      },
    }
  );
};

const getUserReport = () => {
  return axios.get(
    "https://stag-pulse-api-v2.mocogateway.com/v1/idol/report?product=pulse",
    {
      headers: {
        Authorization: "Bearer " + TOKEN,
        "Device-Id": DEVICE_ID,
      },
    }
  );
};

const app = new Vue({
  el: "#app",
  data: () => {
    return {
      loading: true,
      me: {},
      callSession: [],
      friendRequest: [],
      idolOverviewToday: {},
      originalIdolGifts: [],
      userReport: [],
      responseError: "",
      popup: {
        show: false,
        type: "",
      },
    };
  },
  computed: {
    days() {
      const start = moment(this.termBeginning.format("DD MMM YYYY"));
      const end = moment(this.termEnding.format("DD MMM YYYY"));

      let days = [];
      let day = start;

      while (day <= end) {
        days.push(moment(day.toDate()));
        day = day.add(1, "d");
      }

      return days;
    },
    termBeginning() {
      // return moment("29 May 2020, 07:00:00");
      const p = moment(moment().format("16 MMM YYYY, 07:00:00"));
      const currentTime = moment();
      if (currentTime < moment("16 Jun 2020, 07:00:00"))
        return moment("29 May 2020, 07:00:00");
      if (currentTime < p) {
        return moment(moment().format("01 MMM YYYY, 07:00:00"));
      } else {
        return p;
      }
    },
    termEnding() {
      return moment();
      // const p = moment(moment().format("DD MMM YYYY, 07:00:00"));
      // const currentTime = moment();
      // if (currentTime < p) {
      //     return p.subtracts(1, 'd')
      // } else {
      //     return p
      // }
    },
    startOfDay() {
      const p = moment(moment().format("DD MMM YYYY, 07:00:00"));
      const currentTime = moment();

      if (currentTime < p) return p.add(-1, "d");
      else return p;
    },
    idolGifts() {
      const res = [];

      this.originalIdolGifts.forEach((el, ix) => {
        let pos = null;
        const temp = res.filter((e, i) => {
          if (e.gift.id === el.gift.id) {
            pos = i;
            return true;
          }
          return false;
        });

        if (pos === null) {
          res.push(el);
        } else {
          res[pos].count += el.count;
        }
      });

      return res;
    },
  },
  mounted() {
    // const QUERY_PARAMS = this.params();
    // DEVICE_ID = QUERY_PARAMS["device-id"];
    // TOKEN = QUERY_PARAMS["token"];

    this.fetch();
  },
  methods: {
    fetch() {
      let $this = this;

      const from = Math.round(this.termBeginning.valueOf() / 1000);
      const to = Math.round(this.termEnding.valueOf() / 1000);
      const _startOfDay = Math.round(this.startOfDay.valueOf() / 1000);

      this.loading = true;
      getUserInfo()
        .then((res) => {
          if (res.data.status === 0) {
            $this.responseError = "Something went wrong.";
            $this.loading = false;
            return;
          }
          $this.me = res.data.data;
          getCallSession(from, to)
            .then((ress) => {
              $this.callSession = ress.data.data || [];
              // $this.callSession = [];
              getFriendRequest(from, to, res.data.data.user_id)
                .then((resss) => {
                  $this.friendRequest = resss.data.data || [];
                  // $this.friendRequest = [];
                  getIdolOverview(_startOfDay, to)
                    .then((ressss) => {
                      $this.idolOverviewToday = ressss.data.data || {};
                      getUserGift()
                        .then((resssss) => {
                          $this.originalIdolGifts = resssss.data.data || [];
                          // $this.originalIdolGifts = [];
                          getUserReport()
                            .then((ressssss) => {
                              $this.userReport = ressssss.data.data || [];
                            })
                            .catch((e) => {
                              // $this.responseError = "Something went wrong.";
                              $this.loading = false;
                            })
                            .finally(() => {
                              $this.loading = false;
                            });
                        })
                        .catch((e) => {
                          $this.responseError = "Something went wrong.";
                          $this.loading = false;
                        });
                    })
                    .catch((e) => {
                      $this.responseError = "Something went wrong.";
                      $this.loading = false;
                    });
                })
                .catch((e) => {
                  $this.responseError = "Something went wrong.";
                  $this.loading = false;
                });
            })
            .catch((e) => {
              $this.responseError = "Something went wrong.";
              $this.loading = false;
            });
        })
        .catch((e) => {
          $this.responseError = "Something went wrong.";
          $this.loading = false;
        });
    },
    params(search) {
      let _queryParams =
          typeof window !== "undefined" ? window.location.search : search,
        params = {};
      _queryParams = _queryParams ? _queryParams.substring(1) : "";

      if (!_queryParams) return params;

      const splited = _queryParams.split("&");
      splited.forEach((o) => {
        const idx = o.indexOf("=");

        if (idx > 0) {
          const key = o.substring(0, idx),
            value = o.substring(idx + 1, o.length);

          params[key] = value;
        } else {
          params[o] = "";
        }
      });

      return params || {};
    },

    numberFormat(num) {
      let res = "";
      const parts = num.toString().split(".");
      return (
        parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") +
        (parts[1] ? "." + parts[1] : "")
      );
    },
  },
});
