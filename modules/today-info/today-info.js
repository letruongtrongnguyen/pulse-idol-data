Vue.component("today-info", {
  data: () => {
    return {};
  },
  computed: {
    callSessionToday() {
      const data = {
        match: 0,
        valuable_duration: 0,
      };
      const _callSession = this.$root.callSession;

      if (_callSession.length === 0) return data;

      const lastCallSessionItem = _callSession[_callSession.length - 1];

      if (
        moment(lastCallSessionItem.date * 1000).format("DD MMM YYYY") ===
        moment().format("DD MMM YYYY")
      ) {
        return lastCallSessionItem;
      } else return data;
    },
    friendRequestToday() {
      const _friendRequest = this.$root.friendRequest;
      if (!_friendRequest.length) return {};
      return _friendRequest[_friendRequest.length - 1];
    },
    totalGemsToday() {
      return (
        Math.floor(this.callSessionToday.valuable_duration / 60 / 20) * 32 +
        (this.friendRequestToday.accepted + this.friendRequestToday.confirmed) *
          32 +
        this.$root.idolOverviewToday.gems_by_received_gifts
      );
    },
  },
  template: `
    <span>
        <statistic-title
          title="Hôm nay"
          :gems="this.totalGemsToday"
          :note="'Đến ' +  moment().format('hh:mmA, DD/MM')"
        ></statistic-title>

        <div class="daily-info">
          <statistic-item
            title="Thời lượng đạt mốc"
            unit="phút"
            :value="Math.floor(this.callSessionToday.valuable_duration/ 60)"
            icon="resources/images/session-icon.svg"
            :gems="Math.floor(this.callSessionToday.valuable_duration / 60 / 20) * 32"
          ></statistic-item>
          <statistic-item
            title="Kết bạn thành công"
            unit="lượt"
            :value="(this.friendRequestToday.accepted + this.friendRequestToday.confirmed) || 0"
            icon="resources/images/friends.svg"
            :gems="(this.friendRequestToday.accepted + this.friendRequestToday.confirmed) * 32 || 0"            
          ></statistic-item>
          <statistic-item
            title="Quà đã nhận"
            unit="món"
            :value="this.$root.idolOverviewToday.received_gifts || 0"
            icon="resources/images/gift.svg"
            :gems="this.$root.idolOverviewToday.gems_by_received_gifts || 0"            
          ></statistic-item>
          <statistic-item
            title="Số lượng match"
            unit="match"
            :value="this.callSessionToday.match"
            icon="resources/images/phone.svg"
          ></statistic-item>
          <statistic-item
            title="Lời mời kết bạn đã gửi"
            unit="lượt"
            :value="this.friendRequestToday.sent || 0"
            icon="resources/images/mail.svg"
          ></statistic-item>
        </div>
    </span>
	`,
});
