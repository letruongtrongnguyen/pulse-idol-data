Vue.component("report", {
  data: () => {
    return {};
  },
  template: `
		<div class="report" >
			<img src="data:image/svg+xml,%3C%3Fxml version='1.0'%3F%3E%3Csvg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' version='1.1' id='Capa_1' x='0px' y='0px' viewBox='0 0 451.74 451.74' style='enable-background:new 0 0 451.74 451.74;' xml:space='preserve' width='512px' height='512px' class=''%3E%3Cg%3E%3Cpath style='fill:%23FF0000' d='M446.324,367.381L262.857,41.692c-15.644-28.444-58.311-28.444-73.956,0L5.435,367.381 c-15.644,28.444,4.267,64,36.978,64h365.511C442.057,429.959,461.968,395.825,446.324,367.381z' data-original='%23E24C4B' class='' data-old_color='%23E24C4B'/%3E%3Cpath style='fill:%23FFD041' d='M225.879,63.025l183.467,325.689H42.413L225.879,63.025L225.879,63.025z' data-original='%23FFFFFF' class='active-path' data-old_color='%23FFFFFF'/%3E%3Cg%3E%3Cpath style='fill:%23FF0000' d='M196.013,212.359l11.378,75.378c1.422,8.533,8.533,15.644,18.489,15.644l0,0 c8.533,0,17.067-7.111,18.489-15.644l11.378-75.378c2.844-18.489-11.378-34.133-29.867-34.133l0,0 C207.39,178.225,194.59,193.87,196.013,212.359z' data-original='%233F4448' class='' data-old_color='%233F4448'/%3E%3Ccircle style='fill:%23FF0000' cx='225.879' cy='336.092' r='17.067' data-original='%233F4448' class='' data-old_color='%233F4448'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A"/>
			<span>Tố cáo: </span>
			<span>{{this.$root.userReport.length}}</span>
			<span @click="
			   () => {
                  $parent.popup.type = 'report';
                  $parent.popup.show = true;
               }
            ">Xem chi tiết</span>
		</div>
	`,
});
