Vue.component("total-info", {
  data: () => {
    return {};
  },
  computed: {
    beginTime() {
      return this.$root.termBeginning.format("DD/MM");
    },
    endTime() {
      return this.$root.termEnding.format("DD/MM");
    },
    note() {
      return "Từ " + this.beginTime + " - " + this.endTime;
    },
    sessionTimeTotal() {
      const _callSession = this.$root.callSession;

      if (_callSession.length === 0) return 0;

      const sum = _.sumBy(_callSession, (el) => {
        return el.valuable_duration || 0;
      });
      return sum;
    },
    matchTotal() {
      const _callSession = this.$root.callSession;

      if (_callSession.length === 0) return 0;

      const sum = _.sumBy(
        _callSession,
        (el) => {
          return el.match || 0;
        },
        0
      );
      return sum;
    },
    attendance() {
      const _callSession = this.$root.callSession;

      const att = _.sumBy(
        _callSession,
        (el) => {
          return Math.floor(el.valuable_duration / 60) >= 20 ? 1 : 0;
        },
        0
      );
      return att + "/" + this.$root.days.length;
    },
    totalGemByCallSession() {
      return _.sumBy(this.$root.callSession, (c) => {
        return Math.floor(c.valuable_duration / 60 / 20) * 32 || 0;
      });
    },
    totalSuccessFriendRequest() {
      return _.sumBy(this.$root.friendRequest, (el) => {
        return (el.accepted || 0) + (el.confirmed || 0);
      });
    },
    totalSentFriendRequest() {
      return _.sumBy(this.$root.friendRequest, (el) => {
        return el.sent || 0;
      });
    },
    totalGift() {
      return _.sumBy(this.$root.idolGifts, (el) => {
        return el.count;
      });
    },
    totalGemsByGift() {
      return _.sumBy(this.$root.idolGifts, (el) => {
        return el.count * el.gift.gems_value;
      });
    },
    totalGems() {
      return (
        this.totalGemByCallSession +
        this.totalGemsByGift +
        this.totalSuccessFriendRequest * 32
      );
    },
  },
  template: `
    <span>
        <statistic-title
          title="Tổng cộng"
          :gems="this.totalGems"
          :note="this.note"
        ></statistic-title>

        <div style="margin: 0 -7px 8px">
          <statistic-card
            title="Thời lượng match"
            :value="Math.floor(this.sessionTimeTotal / 60) + ' phút'"
            :gems="this.totalGemByCallSession"
            icon="resources/images/session-icon.svg"
            :clicked="() => {
              $parent.popup.type = 'session';
              $parent.popup.show = true;
            }"
          ></statistic-card>

          <statistic-card
            title="Lượt KB thành công"
            :value="this.totalSuccessFriendRequest"
            :gems="this.totalSuccessFriendRequest * 32"
            icon="resources/images/friends.svg"
            :clicked="() => {
              $parent.popup.type = 'add friend successful';
              $parent.popup.show = true;
            }"
          ></statistic-card>

          <statistic-card
            title="Món quà đã nhận"
            :value="this.totalGift"
            :gems="this.totalGemsByGift"
            icon="resources/images/gift.svg"
            :clicked="() => {
              $parent.popup.type = 'gift';
              $parent.popup.show = true;
            }"            
          ></statistic-card>

          <div style="clear: both;"></div>
        </div>

        <div style="margin: 0 -7px 0">
          <statistic-card
            title="Ngày điểm danh"
            :value="this.attendance"
            icon="resources/images/calendar.svg"
            :clicked="() => {
              $parent.popup.type = 'attendance';
              $parent.popup.show = true;
            }"           
          ></statistic-card>

          <statistic-card
            title="Số lượng match"
            :value="matchTotal"
            icon="resources/images/phone.svg"
            :clicked="() => {
              $parent.popup.type = 'match';
              $parent.popup.show = true;
            }"                 
          ></statistic-card>

          <statistic-card
            title="Lượt KB đã gửi"
            :value="this.totalSentFriendRequest"
            icon="resources/images/mail.svg"
            :clicked="() => {
              $parent.popup.type = 'send friend request';
              $parent.popup.show = true;
            }"                 
          ></statistic-card>

          <div style="clear: both;"></div>
        </div>
    </span>
  `,
});
