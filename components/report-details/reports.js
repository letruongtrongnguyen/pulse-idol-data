Vue.component("report-details", {
  data: () => {
    return {
      detailsLoading: true,
    };
  },
  computed: {
    beginTime() {
      return this.$root.termBeginning.format("DD/MM/YYYY");
    },
    endTime() {
      return this.$root.termEnding.format("DD/MM/YYYY");
    },
    totalAmount() {
      let res = 0;
      this.items.forEach((o) => {
        res += o.amount;
      });

      return res;
    },
    totalGems() {
      let res = 0;
      this.items.forEach((o) => {
        res += o.amount * o.value;
      });

      return res;
    },
  },
  mounted() {
    setTimeout(() => {
      this.detailsLoading = false;
    }, 1000);
  },
  template: `
		<div class="report-details details" :class="{loaded: !detailsLoading}">
			<div class="head">
				<h3>Tố cáo</h3>
			</div>
			
			<div class="loader-wrapper" v-if="this.detailsLoading">
	          <loader></loader>
	      	</div>

			<div v-if="!this.detailsLoading" class="list" :style="{marginTop: '15px', overflow: 'auto', padding: 0, maxHeight: 'calc(468px - 135px)'}">
		        <div>
		        	<report-item v-for="item in this.$root.userReport"
		        		:time="moment(item.report.created_at * 1000).format('DD/MM/YYYY - hh:mmA')"
		        		:reportBy="item.reporter_info.user_name"
		        		:reason="item.report.type"
		        	></report-item>
		        </div>
			</div>

		</div>
	`,
});
