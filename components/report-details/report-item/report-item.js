Vue.component("report-item", {
  data: () => {
    return {};
  },
  computed: {
    reasonString() {
      if (!this.reason && this.reason != 0) return "Lý do khác";

      if (typeof this.reason === "string") return this.reason;

      switch (this.reason) {
        case 0:
          return "Tố cáo quấy rối";
        case 1:
          return "Tố cáo khỏa thân";
        case 2:
          return "Tố cáo sai giới tính";
        default:
          return "Lý do khác";
      }
    },
  },
  props: {
    time: String,
    reportBy: String,
    reason: String,
    media: Array,
  },
  template: `
		<div class="report-item" style="font-size: 15px; padding-bottom: 15px; margin-bottom: 20px">
			<div style="margin-bottom: 5px">Ngày: {{time}}</div>
			<div style="margin-bottom: 5px">Người tố cáo: <b style="font-weight: 600; font-size: 16px">{{reportBy}}</b></div>
			<div style="margin-bottom: 5px">Nội dung tố cáo: {{this.reasonString}}</div>

			<div class="report-media" style="margin-top: 15px" v-if="media">
				<div style="width: max-content">
					<div v-for="item in media">
						<img :src="item.url" />
					</div>
				</div>
			</div>
		</div>
	`,
});
