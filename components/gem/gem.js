Vue.component("gem", {
  data: () => {
    return {
      style: {
        container: {
          fontSize: "14px",
        },
        icon: {
          width: "15px",
          height: "14px",
        },
      },
    };
  },
  mounted() {
    this.style.container.fontSize = this.fontsize;
    this.style.icon.width = this.iconwidth;
    this.style.icon.height = this.iconheight;
  },
  props: {
    value: Number,
    fontsize: String,
    iconwidth: String,
    iconheight: String,
  },
  template: `
		<div class="gem" :style="style.container">
			<span>{{this.$root.numberFormat(value)}}</span>
			<img :style="style.icon" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAcCAYAAAB2+A+pAAAABHNCSVQICAgIfAhkiAAABilJREFUSEutlWtsVEUUx8/cuY/d7e52yy62trYVUMtSkBqKpCFAAbEmWJXEmJgQlPBFkQSMiVE/KH4wwUeE4CPywYCQaNQYFFBQMVpM27S2YsW2oKKWR9l2t7DdXbr3zsydMXN3226fQHSTu3Pv3DPnN/9zzpyLYNyv/x3hhavswzzB6se/G/OsiEYQSh4IUTWdXdqlHQhtVTeMt0HjJ/p22jtIgjyTr/EYxkrRpE6FuIQKsAG2GBRJUQwAxmR2FhUdpqYt1L3qxtBTaF+uzRhw9F1RYcVJB0lQw1UM7fkcLwJrEpceaEUudLd8w1OiARFYMRk40s/aVS9e5MrXB9w6rvBvQQPDdmPAl96g35MEqzUTBAru0UyfhRX+J9fHOEWiFRUoDtT5cbBEXPQCwKxcO3sGXLzQni428nVk+DQwAvqemVvQExPA/bvZ49Yg22slCFgJBoxRcCPlRMnt3uUjDgWkUEAkASs3j9kMgV9EKifXBkCqjP0V+cKcbfhVMPw6GH7N1nzakpmbUbtc6yhOvC2CqavsjDVIglaSgjVIIDtG7lpTFEIKqDKLSMAJ8KLRjeTQRZw3AkdLpZ22Sk1rC1T3+X0JSDQyCQUjXwfdr7UXlihL0CPIdsB9b7J95iB9jCQpmIMEcseS233NJfP8NXiecoFHhFSKJy04ji7zKzYghGZ0/RBt8C1zrYg3WSPQYbjh15+euQXtQtG3RK2VIN+TBAWZ28xIsyMBbtqdK56dM1etwldoGwtNe8TSvNlOQtWJD/62Qou9ARIR4JJKfZozOmHP11O6oYZR96b4Z8yis80UAStNgVylYMox++wLG+7Fm2+1ZlTnzbZ+sCL2HzQ1Hbztu96z505fnqN6NDA8Kmh5GhguDHqeBppHAz1PBWzg4+hY6PQ2AXwn4RSs7CW8Nvgr3eAq08BbpsPSFyscFvnJBPoPBdppgR2zgUftCXt4/0hbByX2QlXBoIACqiL/5YhBzmCkcozEaifHX4dOH6CCrR+GO6NNwD/f5cAf/HSRAzBbTOdiFxiItJgAtQhL7N/b6lIVrDuQLExFGBRZoQjLGnh+U3LZDgd8uLjNo1Nvm8VJmHDmQIc3YZRiCMz3wKoXKlOsnXlZ30SVwzvoarnY/PPxczUYSWUZhaPKnQgc3pCoeWDkOMmbb4O/hyliTRYngfFwV6kKK16Z2+O97C2fLr/HXjvZmOw1l+bCMsoVuYkeN9Kr1sXvio8BZ0LeXU/BPmTZNKtYKs9E4NYHQ2cXPzp/zpRgAeLzDY19WChFw2EdVY6GNBUtXxdb5DSPCWA5cSzYtYsA2zoCl2EXDEybkHWfrDSNPNU/GTx+ZrC76bmusISpaLSg5J2m4I0PDNw59UdCOmyDNq0/5PmGcFrr5FrIQstUfNWmOa23rS4Z7dM5O+j9ua+ja3fvwkwhIaeQsor3rI1VjvToKRXLF8dvOlWYtqGDcFaYC/eVu39c83zVsvGKcQGCS50Dke5XI0Uj4c1UcXswmqqphmo6fs2E7/GwwdFQdy3h9BsiqJapcibVRx5+dUkhoEyPV3wI1JAC7mqNkzRT/t4zAFea0jDU6XDiTOVVay/N65ksNVOCpfFXoa5tlk2c5mLyDHz5xtt+n3mL745cZ0kgnUQRlX+9PvK55SDEQ3Wx8OGpinFasFz0ZfC3QyYn9U5XsykUh/0Nd99bNubD/+vHsWZ3pV5zpWkoW7Li5br+8Pbpjt41wQcDJwNYwU2m01woCJV33L++YmGOU9H6USzpKtX85nknxIeboxUPbQfE/xNYLj4SPBWmwm6zbOIhnJHa+lJT17FzrFIx1nm2ZajSOZsCejRmVK2Mz3KaxH8GSwdfBE+tp5wekIVWfoe3ubTMWyPnL3aThmSMy9BTEKKmLhYeaRL/C1g6OTijYxfldCs2eOP8BYGlcq7nN9EpbFEpEDx5X//c966ldNpzPNVi2VzOFeBmymlJuMJTyBnq6zuPCwHQ/rpoxePXC3XSciPGTpUHusqHwGwpCvJBQY1oOq25iJZaXt9bnS3p6/N4w2An5KGTtaqgLxk4z6YYbZqqSfxvOc51dCjw0zZD85ypi1YevT6NY63+BZXNDSswJNO+AAAAAElFTkSuQmCC"/>
		</div>
	`,
});
