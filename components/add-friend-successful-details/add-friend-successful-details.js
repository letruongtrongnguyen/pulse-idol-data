Vue.component("add-friend-successful-details", {
  data: () => {
    return {
      detailsLoading: true,
    };
  },
  computed: {
    beginTime() {
      return this.$root.termBeginning.format("DD/MM/YYYY");
    },
    endTime() {
      return this.$root.termEnding.format("DD/MM/YYYY");
    },
    totalSuccessFriendRequest() {
      return _.sumBy(this.$root.friendRequest, (el) => {
        return (el.accepted || 0) + (el.confirmed || 0);
      });
    },
  },
  mounted() {
    setTimeout(() => {
      this.detailsLoading = false;
    }, 1000);
  },
  props: {},
  template: `
		<div class="add-friend-successful-details details" :class="{loaded: !detailsLoading}">
			<div class="head">
				<h3>Lượt kết bạn thành công</h3>
				<p class="note">({{this.beginTime}} - {{this.endTime}})</p>
			</div>
			
			<div class="loader-wrapper" v-if="this.detailsLoading">
                <loader></loader>
            </div>

			<div v-if="!this.detailsLoading" class="list">
				<div v-if="!!this.$root.friendRequest.length" class="heading">
					<span>Ngày</span>
					<span>Số lượng</span>
					<span>Gems</span>
				</div>
				<div class="scrollable">
                    <div v-for="item in this.$root.friendRequest" class="item">
                        <span>{{moment(item.date * 1000).format("DD/MM")}}</span>
                        <span>{{(item.accepted || 0) + (item.confirmed || 0)}}</span>
                        <span><gem :value="(item.accepted || 0 + item.confirmed || 0) * 32" iconwidth="13px"  iconheight="12px"></gem></span>
                    </div>
				</div>
				<div class="footer">
				    <span>Tổng:</span>
                    <span>{{this.totalSuccessFriendRequest}} lượt</span>
                    <span><gem :value="this.totalSuccessFriendRequest * 32" fontsize="17px" iconwidth="19px"  iconheight="18px"></gem></span>
                </div>
			</div>
		</div>
	`,
});
