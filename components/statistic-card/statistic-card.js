Vue.component("statistic-card", {
  data: () => {
    return {};
  },
  props: {
    name: String,
    icon: String,
    gems: Number,
    title: String,
    value: String,
    // unit: String,
    clicked: Function,
  },
  methods: {
    handleClick() {
      this.clicked && this.clicked(this.name);
    },
  },
  template: `
		<div class="statistic-card" @click="handleClick">
			<img :src="icon" class="icon"/>
			<span>{{value}}</span>
			<span>{{title}}</span>
			<gem v-if="gems || gems === 0" :value="gems"></gem>
		</div>
	`,
});
