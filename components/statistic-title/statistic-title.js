Vue.component("statistic-title", {
  data: () => {
    return {};
  },
  props: {
    title: String,
    gems: Number,
    note: String,
  },
  template: `
		<div class="statistic-title">
			<span>
				<span>{{title}}</span>
				<span v-if="note" class="note">({{note}})</span>
			</span>

			<gem v-if="gems" :value="gems" fontsize="20px" iconwidth="18px" iconheight="17px"/>
		</div>
	`,
});
