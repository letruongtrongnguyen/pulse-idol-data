Vue.component("popup", {
  data: () => {
    return {
      hidding: false,
    };
  },
  props: {
    title: String,
    gems: Number,
    note: String,
    type: String,
    show: Boolean,
    canceled: Function,
  },
  methods: {
    handleCancel() {
      this.hidding = true;
      setTimeout(() => {
        this.canceled && this.canceled();
        this.hidding = false;
      }, 150);
    },
  },
  template: `
		<div class="popup" :class="{hidding: hidding}" v-if="show">
			<div class="overlay" @click="handleCancel"></div>
			<div class="popup-body">
				<session-details v-if="type==='session'"></session-details>
				<add-friend-successful-details v-if="type==='add friend successful'"></add-friend-successful-details>
				<gift-details v-if="type==='gift'" :items="[]"></gift-details>
				<attendance-details v-if="type==='attendance'"></attendance-details>
				<match-details v-if="type==='match'"></match-details>
				<send-friend-request-details v-if="type==='send friend request'"></send-friend-request-details>
        <report-details v-if="type==='report'" :items="[]"></report-details>
			</div>
		</div>
	`,
});
