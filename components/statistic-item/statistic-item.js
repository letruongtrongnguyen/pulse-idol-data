Vue.component("statistic-item", {
  data: () => {
    return {};
  },
  props: {
    title: String,
    value: Number,
    unit: String,
    icon: String,
    gems: Number,
  },
  template: `
		<div class="statistic-item">
			<span>
				<img class="icon" :src="icon"s/>
				<span>{{title}}: <span class="n">{{this.$root.numberFormat(value)}} {{unit}}</span></span>
			</span>

			<gem v-if="gems || gems === 0" :value="gems"/>
		</div>
	`,
});
