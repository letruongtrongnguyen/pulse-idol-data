Vue.component("attendance-details", {
  data: () => {
    return {
      detailsLoading: true,
    };
  },
  computed: {
    beginTime() {
      return this.$root.termBeginning.format("DD/MM/YYYY");
    },
    endTime() {
      return this.$root.termEnding.format("DD/MM/YYYY");
    },
    items() {
      const res = [];
      const _days = this.$root.days;
      const _callSession = this.$root.callSession;

      _days.forEach((el, ix) => {
        const d = _callSession.filter((c) => {
          return (
            moment(el).format("DD MMM YYYY") ===
            moment(c.date * 1000).format("DD MMM YYYY")
          );
        })[0];

        if (!d) {
          res.push({
            date: el,
            match: 0,
            valuable_duration: 0,
          });
        } else {
          res.push({
            date: el,
            match: d.match,
            valuable_duration: d.valuable_duration,
          });
        }
      });

      return res;
    },
    attendance() {
      const att = _.sumBy(
        this.items,
        (el) => {
          return Math.floor(el.valuable_duration / 60) >= 20 ? 1 : 0;
        },
        0
      );
      return att + "/" + this.items.length;
    },
  },
  mounted() {
    setTimeout(() => {
      this.detailsLoading = false;
    }, 1000);
  },
  props: {},
  template: `
		<div class="attendance-details details" :class="{loaded: !detailsLoading}">
			<div class="head">
				<h3>Điểm danh</h3>
				<p class="note">({{this.beginTime}} - {{this.endTime}})</p>
			</div>
			
			<div class="loader-wrapper" v-if="this.detailsLoading">
                <loader></loader>
            </div>			

			<div v-if="!this.detailsLoading" class="list">
				<div v-if="!!this.items" class="heading">
					<span style="text-align: center !important">Ngày</span>
					<span>Thời lượng</span>
					<span>Điểm danh</span>
				</div>
				<div class="scrollable">
                    <div v-for="(item, index) in items" class="item">
                        <span style="text-align: center !important">{{moment(item.date).format("DD/MM")}}</span>
                        <span>{{Math.floor(item.valuable_duration / 60)}}p</span>
                        <span style="text-align: center !important" >
                            <img v-if="Math.floor(item.valuable_duration / 60) >= 20" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='16.751' height='12.972' viewBox='0 0 16.751 12.972'%3E%3Cg id='prefix__Group_26317' data-name='Group 26317' transform='translate(.5 .581)'%3E%3Cg id='prefix__Group_26316' data-name='Group 26316'%3E%3Cpath id='prefix__Path_48406' fill='%2313c66c' stroke='%2313c66c' d='M117.64 137.019a.984.984 0 0 0-1.368 0l-9.148 9.148-3.242-3.242a.984.984 0 0 0-1.416 1.368l.024.024 3.938 3.938a.984.984 0 0 0 1.392 0l9.844-9.844a.984.984 0 0 0-.024-1.392z' data-name='Path 48406' transform='translate(-102.19 -136.743)'/%3E%3C/g%3E%3C/g%3E%3C/svg%3E%0A">
                            <img v-if="Math.floor(item.valuable_duration / 60) < 20 && moment(item.date).format('DD MMM') !== moment().format('DD MMM')" src="data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='15.558' height='15.556' viewBox='0 0 15.558 15.556'%3E%3Cdefs%3E%3Cstyle%3E .prefix__cls-1%7Bfill:%23ef0000%7D %3C/style%3E%3C/defs%3E%3Cg id='prefix__Group_26319' data-name='Group 26319' transform='translate(-273.999 -308)'%3E%3Crect id='prefix__Rectangle_7224' width='3' height='19' class='prefix__cls-1' data-name='Rectangle 7224' rx='1.5' transform='rotate(45 -228.072 500.964)'/%3E%3Crect id='prefix__Rectangle_7225' width='3' height='19' class='prefix__cls-1' data-name='Rectangle 7225' rx='1.5' transform='rotate(135 78.207 220.687)'/%3E%3C/g%3E%3C/svg%3E%0A">
                        </span>
                    </div>
				</div>
				<div class="footer">
				    <span style="padding-left: 20px">Tổng:</span>
                    <span style="text-align: right; padding-right: 10px">{{this.attendance}} ngày</span>
                </div>
			</div>
		</div>
	`,
});
