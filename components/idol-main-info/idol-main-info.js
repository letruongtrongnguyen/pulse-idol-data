Vue.component("idol-main-info", {
  data: () => ({}),
  props: ["name", "gems", "avatar"],
  template: `
        <div class="idol-main-info">
            <span class="avatar">
                <img :src="avatar">
            </span>
            <div class="info">
                <span>{{this.name}}</span>
                <span>Số dư hiện tại:</span>
                <gem :value="gems" fontsize="27px" iconwidth="24px" iconheight="23px"></gem>
            </div>
        </div>
    `,
});
