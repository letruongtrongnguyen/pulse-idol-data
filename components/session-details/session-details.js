Vue.component("session-details", {
  data: () => {
    return {
      detailsLoading: true,
    };
  },
  computed: {
    beginTime() {
      return this.$root.termBeginning.format("DD/MM/YYYY");
    },
    endTime() {
      return this.$root.termEnding.format("DD/MM/YYYY");
    },
    items() {
      const res = [];
      const _days = this.$root.days;
      const _callSession = this.$root.callSession;

      _days.forEach((el, ix) => {
        const d = _callSession.filter((c) => {
          return (
            moment(el).format("DD MMM YYYY") ===
            moment(c.date * 1000).format("DD MMM YYYY")
          );
        })[0];

        if (!d) {
          res.push({
            date: el,
            match: 0,
            valuable_duration: 0,
          });
        } else {
          res.push({
            date: el,
            match: d.match,
            valuable_duration: d.valuable_duration,
          });
        }
      });

      return res;
    },
    sumOfSessionTime() {
      return _.sumBy(this.items, (el) => {
        return el.valuable_duration || 0;
      });
    },
    totalGem() {
      return _.sumBy(this.items, (el) => {
        return Math.floor(el.valuable_duration / 60 / 20) * 32 || 0;
      });
    },
  },
  mounted() {
    setTimeout(() => {
      this.detailsLoading = false;
    }, 1000);
  },
  props: ["data"],
  template: `
		<div class="session-details details" :class="{loaded: !detailsLoading}">
			<div class="head">
				<h3>Thời lượng match</h3>
				<p class="note">({{this.beginTime}} - {{this.endTime}})</p>
			</div>
			
			<div class="loader-wrapper" v-if="this.detailsLoading">
                <loader></loader>
            </div>

			<div v-if="!this.detailsLoading" class="list">
				<div v-if="!!this.items.length" class="heading">
					<span>Ngày</span>
					<span>Thời lượng</span>
					<span>Gems</span>
				</div>
				<div class="scrollable">
                    <div v-for="item in items" class="item">
                        <span>{{moment(item.date).format("DD/MM")}}</span>
                        <span>{{(Math.floor(item.valuable_duration / 60))}}p</span>
                        <span><gem :value="Math.floor(item.valuable_duration / 60 /20) * 32" iconwidth="13px"  iconheight="12px"></gem></span>
                    </div>
				</div>
				<div class="footer">
				    <span>Tổng:</span>
                    <span>{{Math.floor(sumOfSessionTime / 60)}}p</span>
                    <span><gem :value="this.totalGem" fontsize="17px" iconwidth="19px"  iconheight="18px"></gem></span>
                </div>
			</div>
		</div>
	`,
});
