Vue.component("gift-item", {
  data: () => {
    return {
      
    };
  },
  props: {
    name: String,
    amount: Number,
    value: Number,
    icon: String,
  },
  template: `
		<div class="gift-item">
			<div class="icon">
        <img :src="icon" />
      </div>
      <div class="name">
        <span>{{name}}</span> x{{amount}}
      </div>
      <div class="value">
        <gem :value="amount * value"></gem>
      </div>
		</div>
	`,
});
