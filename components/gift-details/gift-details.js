Vue.component("gift-details", {
  data: () => {
    return {
      detailsLoading: true,
    };
  },
  computed: {
    beginTime() {
      return this.$root.termBeginning.format("DD/MM/YYYY");
    },
    endTime() {
      return this.$root.termEnding.format("DD/MM/YYYY");
    },
    totalAmount() {
      let res = 0;
      this.$root.idolGifts.forEach((o) => {
        res += o.count;
      });

      return res;
    },
    totalGems() {
      let res = 0;
      this.$root.idolGifts.forEach((o) => {
        res += o.count * o.gift.gems_value;
      });

      return res;
    },
  },
  mounted() {
    setTimeout(() => {
      this.detailsLoading = false;
    }, 1000);

    console.log(this.$root.idolGifts);
  },
  template: `
		<div class="gift-details details" :class="{loaded: !detailsLoading}">
			<div class="head">
				<h3>Món quà đã nhận</h3>
				<p class="note">({{this.beginTime}} - {{this.endTime}})</p>
			</div>
			
			<div class="loader-wrapper" v-if="this.detailsLoading">
          <loader></loader>
      </div>

			<div v-if="!this.detailsLoading" class="list" :style="{marginTop: '15px', overflow: 'auto', padding: 0, maxHeight: 'calc(468px - 140px)'}">
        <div>
				  <gift-item v-for="item in this.$root.idolGifts"
            :name="item.gift.name"
            :value="item.gift.gems_value"
            :amount="item.count"
            :icon="item.gift.extension.thumb_url"
          ></gift-item>

          <div style="clear: both"></div>
        </div>
			</div>

      <div v-if="!this.detailsLoading" class="total" :style="{
        padding: '15px 15px 0',
        borderTop: '1px solid #ddd',
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center',
      }">
        <span>Tổng:</span>
        <span style="font-size: 17px; font-weight: 600">{{totalAmount}} món</span>
        <span>
          <gem :value="totalGems" fontsize="17px" iconwidth="17px" iconheight="16px"></gem>
        </span>
      </div>
		</div>
	`,
});
